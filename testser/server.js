var express = require('express');
var io = require('socket.io');
var bodyParser = require('body-parser');
var sqlite3 = require('sqlite3').verbose();
var TransactionDatabase = require("sqlite3-transactions").TransactionDatabase;
var fs = require('fs');
var Mecab = require('mecab-async');
var async = require('async');
var mecab = new Mecab();
var Ngram = require('./Ngram');
var ngram = new Ngram("3");
var app = express();
var obj,server;
var count_eng = 0;
var os = require('os');
var port = 1337;

var treetagger = require('treetagger');
var tagger = new treetagger();
//tagger.tag("This is a test!", function (err, results) {
//    console.log(results);
//});


//挿入コマンド
var dbInsert = 'INSERT INTO main VALUES (?, ?, ?, ?)';
var dbInsertENG = 'INSERT INTO ENGwords VALUES (?, ?, ?)';
var dbInsertJAP = 'INSERT INTO JAPwords VALUES (?, ?, ?)';
//テーブルクリエイトコマンド
var dbCreate = 'CREATE TABLE IF NOT EXISTS main(parID, senID, sentEng, sentJap)';
var dbCreateENG = 'CREATE TABLE IF NOT EXISTS ENGwords(senID, wordsEng, pos)';
var dbCreateJAP = 'CREATE TABLE IF NOT EXISTS JAPwords(senID, wordsJap, pos)';
//デリートコマンド
var dbDelete = '';
//サーチコマンド
/*
	SELECT *でとってくるよりもSELECT (カラム名)とカラム名を指定してとった方が速いらしいので，変更。
	尚、どれぐらい変わるかは不明。
*/
var dbSearchAll = 'SELECT parId, sentEng, sentJap FROM main';
var dbSearchENG = 'SELECT senID, wordsEng, pos FROM ENGwords WHERE wordsEng = ?';
var dbSearchJAP = 'SELECT senID, wordsJap, pos FROM JAPwords WHERE wordsJap = ?';
var dbSearch = 'SELECT parID, senID, sentEng, sentJap FROM main WHERE senID = ?';

//var db = new sqlite3.Database("ParallelText.sqlite3");
var db = new TransactionDatabase(
  new sqlite3.Database("ParallelText.sqlite3")
);

app.use(bodyParser.json());
app.use(express.static("public"));
app.use(function(req,res,next){
    res.status(404);
    res.end("Not Found");
});
/*ajaxの時使った奴
app.post("/",function(req,res){
    console.log(req.body);
    res.send(req.body);
});
*/
/* added by onoaki ------------------------------ */
// 拾い物。ローカルIPアドレスを取得するやつ。
// これで、app.listenを切り替える必要がなくなるんじゃないか。
// なお、localhostで入れない模様。
// IPAddress[ "ipv4" or "ipv6"][0].address でIPアドレスがとれる。
var IPAddress = getLocalAddress();
console.log("IPアドレス: "+IPAddress["ipv4"][0].address);
console.log("ポート番号: "+port);

function getLocalAddress() {
    var ifacesObj = {}
    ifacesObj.ipv4 = [];
    ifacesObj.ipv6 = [];
    var interfaces = os.networkInterfaces();
	//console.log(interfaces);
    for (var dev in interfaces) {
        interfaces[dev].forEach(function(details){
	    //console.log(details);
            if (!details.internal){
                switch(details.family){
                case "IPv4":
                    ifacesObj.ipv4.push({name:dev, address:details.address});
                    break;
                case "IPv6":
                    ifacesObj.ipv6.push({name:dev, address:details.address})
                    break;
                }
            }
        });
    }
    return ifacesObj;
};
/* ---------------------------------------------- */
//サーバー起動
server = app.listen(port, IPAddress["ipv4"][0].address);

console.log("server running...");
async.series([
    //コーパスをDBに入れる
    function(callback){
	callback(null,"third");
    },function(callback){
	console.log("last");
	callback(null,"last");
    }
],function (err, results) {
    if (err) {
        throw err;
    }
    console.log('series all done. ' + results);
});

var socket = io.listen(server);
socket.on('connection', function(client) {
  // connect
    //検索要求が来る．data.valueが検索クエリ
    client.on('C_to_S_search', function(data){
	
	console.log("query: "+data.value);
	//DBを検索．検索クエリにあうものを探す入力:data.value 出力:senID
	//ENGwordsを検索
	//クエリーの言語を判定
	if(DetermineLanguage(data.value)){
	    //英語で検索
	    SearchDB(dbSearchENG, data.value, client, "English");
	}else{
	    //日本語で検索
	    SearchDB(dbSearchJAP, data.value, client, "Japanese");
	}
    });
    // ============== added by sakihiro ================//
    //類文検索を行う 
    //arg:data は1sentence
    //DBの全てのsentenceとクエリsentenceの類似度を計算
    // =================================================//
    client.on('C_to_S_search_sentence', function(data){
	Calculate(data.value, function(results){
	    client.emit('S_to_C_search_sentence', results);
	});
    });
    // added by masatow ========================================== //
    // arg:data JSON形式　段落ID，文ID，英文，日本語文を保持
    client.on('register',function(data){
        // 段落ID，文ID，英文，日本語文をそれぞれ変数に格納
        // parID,senID,senEng,senJap,wordEng,wordJap に分解する必要あり？　小野田まかせた
        var sectionID = data.sectionID;
        var sentenceID = data.sentenceID;
        var textJap = data.textJap;
        var textEng = data.textEng;
	
	console.log("受け取ったデータ パラグラフID: " + sectionID + "senID: " + sentenceID + "senEng: " + textEng + "senJap" + textJap);
	db.all("SELECT parID,senID FROM main WHERE parID = '"+sectionID+"' or senID = '"+sentenceID+"'", function(err, rows){
	    if(!err){
		if(rows.length == 0){
		    console.log("ID被りは無いようだ。");
		    db.serialize(function(){
			var stmt = db.prepare(dbInsert);
			stmt.run(sectionID, sentenceID, textEng, textJap);
			stmt.finalize();
			var wordsEng = textEng.split(" ");
			stmt = db.prepare(dbInsertENG);
			for(var i=0; i<wordsEng.length; i++){
			    stmt.run(sentenceID,wordsEng[i]);
			}
			stmt.finalize();
			var wordsJap = mecab.wakachiSync(textJap);
			stmt = db.prepare(dbInsertJAP);
			for(var i=0; i<wordsJap.length; i++){
			    stmt.run(sentenceID, wordsJap[i]);
			}
			stmt.finalize();
			console.log("ok!!!");
		    });
		}else{
		    console.log("ぶーわ。ID被っとるわ。");
		}
	    }else{
		console.log(err);
	    }
	});
        // データベースに重複がないかチェック．引数わからん．小野田まかせた
        // SearchDB(dbSearchENG, data.value, client, "English"); 
	// SearchDB(dbSearchJAP, data.value, client, "Japanese");
        
        // データベースにINSERT 関数すらわからん　小野田まかせた
        // db.prepare(dbInsert);
    });
    // =========================================================== //
    // added by onoaki ------------------------------------------- //
    // arg: data parIDを保持。
    client.on('C_to_S_parIDsearch', function(data){
	var query = data.query;
	db.all("SELECT parID, senID, sentEng, sentJap FROM main WHERE parID = '"+query+"'", function(err, rows){
	    client.emit("S_to_C_parIDsearch", rows);
	});
    });
    // ----------------------------------------------------------- //
    client.on('message', function(message) {
	cosole.log(message);
    });
    client.on('disconnect', function() {
	// disconnect
    });
});

/*
 クエリーが日本語か英語か判定。現状での判定方法は以下の通りである。
 判定方法：
 クエリーの中に多バイト文字が一つでもあったら⇒日本語
 全部が1バイト文字だったら⇒英語
 返り値:
 true クエリーは英語であると判定。
 false クエリーは日本語であると判定。
 */
function DetermineLanguage(query){
    for(var i=0; i<query.length; i++){
	if(query.charCodeAt(i) >= 256){
	    console.log("クエリーは日本語だね。");
	    return false;
	}
    }
    //全てが1バイト文字だったらfalseを返す。
    console.log("クエリーは英語だね。");
    return true;
}

/*
 読みづらすぎたので関数化。
 引数:
 command: SQLite3のコマンド
 query: 検索クエリー
 client: emitするためにclient
 lan: クエリーの言語("English" or "Japanese")
 */
function SearchDB(command, query, client, lan){
    var resultID = [];
    var resultSen = [];
    var result = [];
    db.each(command, query, function(err, row) {
	if(err){
	    console.log("errorrrrrrrrr");
	}
	if(resultID.indexOf(row.senID) >= 0) {
	    console.log("senID:" +row.senID + " 登録済み");
	}
	else {
	    console.log("senID:"+row.senID);
	    resultID.push(row.senID);
	}
    },function(err,rows){
	if(err) {
	    throw err;
	}
	console.log("search DB finish");
	//○○words内に検索結果
	//resultIDと一致するセンテンスを持ってくる．
	async.each(resultID,function(id,callback){
	    //console.log("id check: "+id);
	    db.each(dbSearch,id,function(err,row){
		//resultSen.push(row.sentEng);
		result.push(row);
	    },function(err){
		callback();
	    });
	},function(err){
	    if(err){
		throw err;
	    }
	    //console.log(result.length);
	    client.emit("S_to_C_search", {result:result, count: rows, lang: lan});  
	});
    });
}
/*
queryとデータベース内の各センテンスとの類似度を計算
*/
function Calculate(query, callback) {
    var results = [];
    var textEng,textJap,parID;
    db.each(dbSearchAll,function(err, row){
	//各文とクエリの類似度計算
	parID = row["parID"];
	textEng = row["sentEng"];
	textJap = row["sentJap"];
	agreeValue(query, textEng, function(agreement){
	    var total = parseFloat(agreement) + parseFloat(ngram.compare(query, textEng));
	    results.push({parID:parID, textEng:textEng, textJap:textJap, value:total});	    
	});
    },function(err, rows){
	//resultsをvalueでソート
	results = mySort(results, "value");
	callback(results);
	//return results;
    });
}

function mySort(json, key) {
    var tmp = [];
    var sorted = [];
    for(var i=0; i<json.length;i++) {
	tmp[i] = [json[i][key], json[i]];
    }
    tmp.sort();
    for(var j=0; j<json.length; j++) {
	sorted[json.length - j -1] = tmp[j][1];
    }
    
    var result ="";
    return sorted;
}
/*
text1とtext2に同じ単語がいくつ含まれているかをカウントする
*/
function agreeValue(text1, text2, callback){
    text1 = RemakeText(text1);
    text2 = RemakeText(text2);
    var words_text1 = text1.split(" ");
    var words_text2 = text2.split(" ");
    var agreement = 0;
    var count = 0;
    async.each(words_text1, function(word, callback1){
	if(words_text2.indexOf(word) > 0) {//text2が単語(word)を含んでいる場合
	    agreement++;
	}
	callback1();
    }, function(err){
	if(err) throw err;
	callback(agreement / words_text2.length);
    });
}
function RemakeText(text){
    //テキストから[. , " ']などを除去
    text = text.replace(/\./, "");
    text = text.replace(/,|\"/g, " ");
    //複数空白がある場合一つにする
    text = text.replace(/\s+/g, " ");
   
    return text;
}
