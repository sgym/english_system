var sqlite3 = require('sqlite3').verbose();
var fs = require('fs');
var async = require('async');
var TransactionDatabase = require("sqlite3-transactions").TransactionDatabase;
var treetagger = require('treetagger');
var tagger = new treetagger();
var Mecab = require('mecab-async');
var mecab = new Mecab();
//挿入コマンド
var dbInsert = 'INSERT INTO main VALUES (?, ?, ?, ?)';
var dbInsertENG = 'INSERT INTO ENGwords VALUES (?, ?, ?)';
var dbInsertJAP = 'INSERT INTO JAPwords VALUES (?, ?, ?)';
//テーブルクリエイトコマンド
var dbCreate = 'CREATE TABLE IF NOT EXISTS main(parID, senID, sentEng, sentJap)';
var dbCreateENG = 'CREATE TABLE IF NOT EXISTS ENGwords(senID, wordsEng, pos)';
var dbCreateJAP = 'CREATE TABLE IF NOT EXISTS JAPwords(senID, wordsJap, pos)';
var db = new TransactionDatabase(
  new sqlite3.Database("ParallelText.sqlite3")
);
loadCorpus();

//対訳コーパスの読み込み 
function loadCorpus(){
    fs.readdir('./public/ParallelTexts', function(err, files){
	if (err) throw err;
	var fileList = [];
	var filePath = "";
        db.serialize(function(){
            //table作成
	    console.log("テーブル作成");
	    db.run(dbCreate);
	    db.run(dbCreateENG);
	    db.run(dbCreateJAP);
	    files.forEach(function(file){
                filePath = "./public/ParallelTexts/" + file;
                InsertDataToDB2(filePath);
	    });
        });	    
    });
}

function InsertDataToDB2(filePath){
    var parID,senID,senEng,senJap,wordEng,wordJap;
    var parIDArr =[],senIDArr=[],senEngArr=[],senJapArr=[];
    
    var fileContent = fs.readFileSync(filePath, 'utf8');
    var lines = fileContent.trim().split('\r');
    var stmt1,stmt2,stmt3, stmt;
    var sentenceArr = [];
    var counter = 0;
    db.beginTransaction(function(err, transaction){
	console.log(filePath + " 形態素解析 & Insertスタート");
	//行単位で分割
	for(var i = 0; i< lines.length;i++){
	    var line = lines[i];
	    var tmp = line.split('\t');
	    if(tmp[2] && tmp[2] != "" && tmp[2] != "英文"){
		//日本語文の"'を削除。
		tmp[3] = tmp[3].replace(/\"|\'/g,"");
		/* sentenceArr:
                 要素:
		 parID :parIDを格納。
		 senID :senIDを格納。
		 senEng:一つの英文を格納。
		 senJap:一つの日本語文を格納。
		 */
		sentenceArr.push({parID:tmp[0], senID:tmp[1], senEng:tmp[2], senJap:tmp[3]});
		transaction.run(dbInsert,[tmp[0],tmp[1],tmp[2],tmp[3]]);
	    }
	}
	//特殊文字(,."':;)削除
	//したいけど上手くいかない
	// "."が特殊文字だから\入れればいけるんじゃない？
	//buf = buf.replace(/./g,"");
	
        //英文の形態素解析及びDBへのInsert
        async.eachLimit(sentenceArr, 30, function(buf, callback){
            var buf_eng = buf.senEng;
            var senid_eng = buf.senID;
            tagger.tag(buf_eng,function(err,results){
                if(err) throw err;
                for(var i=0; i<results.length; i++){
                    if(i == results.length -1){
                        transaction.run(dbInsertENG, [senid_eng, results[i].t, results[i].pos], function(){
                            callback();
                        });
                    }
                    else {
                        transaction.run(dbInsertENG, [senid_eng, results[i].t, results[i].pos]);
                    }
                }
            });

	    var buf_jap = buf.senJap;
            var senid_jap = buf.senID;
	    mecab.parse(buf_jap, function(err, results){
		if (err) throw err;
		for(var l=0;l<results.length;l++){
                    //記号をフィルタリング
                    if(results[l][1] != "記号"){
			if(l == results.length -1 ) {
			    transaction.run(dbInsertJAP,[senid_jap, results[l][0], results[l][1]], function(){
				callback();
			    });
			}
			else {
			    transaction.run(dbInsertJAP,[senid_jap, results[l][0], results[l][1]]);
			}
		    }
		}
	    });
        });
/**
	async.eachLimit(sentenceArr, 30, function(buf, callback){
            var buf_jap = buf.senJap;
            var senid_jap = buf.senID;
	    mecab.parse(buf_jap, function(err, results){
		console.log(results);
		if (err) throw err;
		for(var l=0;l<results.length;l++){
                    //記号をフィルタリング
                    if(results[l][1] != "記号"){
			if(l == results.length -1 ) {
			    transaction.run(dbInsertJAP,[senid_jap, results[l][0], results[l][1]], function(){
				callback();
			    });
			}
			else {
			    transaction.run(dbInsertJAP,[senid_jap, results[l][0], results[l][1]]);
			}
		    }
		}
	    });
        });
**/
        // Remember to .commit() or .rollback()
        transaction.commit(function(err) {
            if (err) return console.log("Sad panda :-( commit() failed.", err);
            console.log("Happy panda :-) commit() was successful.");
	});
        // or transaction.rollback()

        console.log(filePath + " finish");
    });
}
