var socket = io.connect('http://133.68.112.110:1337');
//var socket = io.connect('http://localhost:1337');

var data, query;

$(function(){
    //サーバから受け取るイベント
    socket.on("connect", function () {});  // 接続時
    socket.on("disconnect", function (client) {});  // 切断時
    $("#send_registration").on("click",function(){
	send_register();
    });
});
function send_register(query){
    var parID = $("#parID").val();
    var senID = $("#senID").val();
    var senEng = $("#senEng").val();
    var senJap = $("#senJap").val();
    if(parID =="" || senID =="" || senEng == "" || senJap =="") {
	alert("空欄がありまぁす");
    }
    else {
	console.log("パラグラフID: " + parID + "センテンスID: " + senID + "英文: " + senEng + "日本語文: " + senJap);
	socket.emit("register",{sectionID:parID, sentenceID:senID, textEng:senEng, textJap:senJap});
    }
};
function receive_search(){
    if(!isSearch_flag) {
	isSearch_flag = true;
	$("#search").removeAttr("disabled");
	$("#word").removeAttr("disabled");
	$("#word").value = "";
    }
};
