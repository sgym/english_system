var socket = io.connect('http://133.68.112.110:1337');
//var socket = io.connect('http://localhost:1337');

var data, query;
var isSearch_flag =true; //検索しているかのフラグ

$(function(){
    //サーバから受け取るイベント

    socket.on("connect", function () {});  // 接続時
    socket.on("disconnect", function (client) {});  // 切断時
    socket.on("S_to_C_search", function (data) {//用例検索
	receive_search();
	//検索結果の件数
	var count_result = parseInt(data.count);
	console.log(count_result);
	//結果なしの場合の処理
	if(count_result == 0) {
	    displayBlank(query);
	}
	else{
	    console.log(data);
	    displayResult(data.result,query);
	}
    });
    socket.on("S_to_C_search_sentence", function(data){
	receive_search();
	if(data) {
	    displaySimilarSentence(data);
	}
	else {
	    displayBlank(query);
	}
    });
    socket.on("S_to_C_parIDsearch",function(data){
	console.log(data);
	//popupに表示する文章を追加
	initPopUp(data);
	book_mode_start();
    });
    //用例検索
    $("#word").keypress(function(e){
	if (e.charCode == 13) {
	    query = $("#word").val();
	    send_search(query);
	}
    });
    $("#search_word").on("click",function(){
	query = $("#word").val();
	send_search(query);
    });
    //類文検索
    $("#sentence").keypress(function(e){
	if (e.charCode == 13) {
	    query = $("#sentence").val();
	    send_search_sentence(query);
	}
    });
    $("#search_sentence").on("click",function(){
	query = $("#sentence").val();
	send_search_sentence(query);
    });
    //ポップアップクローズボタン
    $(".close_btn, #overlay").on("click", function(){
	$(".popup, #overlay").hide();
    });    
});
function initPopUp(data){
    $(".popup_inner").html("");
    $(".popup_inner").on("scroll",null);
    data.forEach(function(result){
	var sentEnglish = result.sentEng;
	var sentJapanese = result.sentJap;
	$(".popup_inner").append("<p>" + sentEnglish + "<br>" + sentJapanese + "</p>");
    });
}
function displayBlank(SearchTerm){
    $("#query").html("");
    $("#results").html("");
    $("#results").append("<b>"+ SearchTerm +"</b>に一致する用例は見つかりませんでした．");
};
function displayResult(obj,SearchTerm){
    var num = 0;
    var test = new RegExp("([^A-Za-z]+?)("+ SearchTerm +")([^A-Za-z]+?)");
    $("#results").html("");
    obj.forEach(function(result){
	var sentEnglish = result.sentEng;
	var sentJapanese = result.sentJap;
	$("#results").append(
	    "<div class=ond_object>"
		+"<img src=../images/book_icon.jpeg onClick=book_mode_request(event) />"
		+"<div id=sentenceond parID=" +result.parID+ "><li><b>" + sentEnglish.replace(test,"$1<mark>$2</mark>$3") + "<br>" + sentJapanese.replace(test,"$1<mark>$2</mark>$3") + "</b></li></div></div>"
		+"<br><br>"
	);
    });
};
function book_mode_request(event){
    console.log($(event.target).next().attr("parid"));
    var parID = $(event.target).next().attr("parid");
    socket.emit("C_to_S_parIDsearch",{query:parID});
}
function book_mode_start(){
    console.log("popup表示");
    // marginを設定して表示
    $('.popup').show();
    $('#overlay').show();
}
function send_search(query){
    if(query == ""){
	alert("空白での検索？認められないわぁ");
    }
    else {
	if(isSearch_flag) {
	    isSearch_flag = false;
//	    $("#search").attr("disabled","disabled");
//	    $("#word").attr("disabled","disabled");
	    $("#search_object").attr("disabled", "disabeld");
	    socket.emit("C_to_S_search",{value:query});
	}
    }
};
function send_search_sentence(query){
    var words = query.split(" ");
    if(query == ""){
	alert("空白での検索？認められないわぁ");
    }
    else if(words.length < 3){
	alert("文章みじかすぎぃ！");
    }
    else if(isSearch_flag) {
	isSearch_flag = false;
	//$("#search").attr("disabled","disabled");
	//$("#word").attr("disabled","disabled");
	console.log(query);
	$("#search_object").attr("disabled", "disabeld");
	socket.emit("C_to_S_search_sentence",{value:query});
    }
};
function receive_search(){
    if(!isSearch_flag) {
	isSearch_flag = true;
//	$("#search").removeAttr("disabled");
//	$("#word").removeAttr("disabled");
	$("#search_object").removeAttr("disabled");
	$("#word").value = "";
	$("#sentence").value = "";
    }
};
function displaySimilarSentence(data){
    $("#query").html("検索文: ");
    $("#results").html("");
    var words = query.split(" ");
    words.forEach(function(result){
	$("#query").append(result + " ");
    });
    data.forEach(function(result){
	var sentEnglish = result.textEng;
	var sentJapanese = result.textJap;
	var value = result.value;
	$("#results").append(
	    "<div class=ond_object>"
		+"<img src=../images/book_icon.jpeg onClick=book_mode_request(event) />"
		+"<div id=sentenceond parID=" +result.parID+ "><li><b>" + sentEnglish + "<br>" + sentJapanese + "</b>"+ result.value +"</li></div></div>"
		+"<br><br>"
	);
    });
};
