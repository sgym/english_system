var async = require('async');
var ngram;
var Ngram = function(n){
    ngram = n;
};
function RemakeText(text){
    //テキストから[. , " ']などを除去
    text = text.replace(/\./, "");
    text = text.replace(/,|\"/g, " ");
    //複数空白がある場合一つにする
    text = text.replace(/\s+/g, " ");
   
    return text;
}
function CreateNgram(text){
     var stopwords = ["a", "A", "the", "The", "an", "An", "I", "You", "you", "We", "we", "he", "He", "she", "She", "is", "are", "am"];
    //N-gramのNが何か
    var n = ngram;
    var text1 = RemakeText(text);
    //テキストを空白で分割
    var words = text1.split(' ');
    if(words.length < n){
	return null;
    }
    var tmp;
    var results = [];
    for(var i = 0; i<words.length - n + 1; i++){
	tmp = [];
	//iからn個の語の組を作る
	for(var num = 0; num < n; num++){
	    if(stopwords.indexOf(words[i + num]) < 0){
		//words[i + num]がストップワードではない場合
		if(words[i + num]){
		    tmp.push(words[i + num]);	
		}
	    }
	    if(num == n-1){
		results.push(tmp);
	    }
	}
    }
    return results;
}

//二つの文章を比較
Ngram.prototype.compare = function(text1, text2){
    var ngram1 = CreateNgram(text1);
    var ngram2 = CreateNgram(text2);
    var n1_length = ngram1.length;
    var score = 0;
    if(!ngram2) {
	return 0;
    }
    else {

	var n2_length = ngram2.length;
    }
    //短い方をngram1に，長い方をngram2に入れる
    if((n1_length - n2_length) > 0){
	//n1の方が長い
	var tmp = ngram1; 
	ngram1 = ngram2;
	ngram2 = tmp;
    }

    var n1n2_abs = Math.abs(n1_length - n2_length);
    var all_cell = Math.min(n1_length, n2_length)*(Math.min(n1_length, n2_length) + 1)/2 + n1n2_abs * Math.min(n1_length, n2_length);

    for(var i = 0; i< ngram1.length;i++){
	for(var j = i; j < ngram2.length; j++){
	    for(var k = 0; k < ngram;k++){
		if(ngram1[i][k] == ngram2[j][k] && ngram1[i][k] && ngram2[j][k]){
		    score++;
		}
	    }
	}
    }
    score = score / all_cell;
    return score;
};
//テキストのN-gramを作成
Ngram.prototype.tokens = function(text){
    return CreateNgram(text);
};
module.exports = Ngram;
